# producer.py

# TODO: revisar uvloop por performance de event loop
import asyncio
from aioredis import create_connection, Channel
import websockets

from conf import REDIS_HOSTNAME, REDIS_PORT


servicios_a_claves = {
    'contactados': 'CONTACTED',
    'calificados': 'DISPOSITIONED',
    'no_atendidos': 'NOT_ATTENDED'
}


def path_to_key(path):
    __, __, __, nombre_servicio, __, task_id = path.split('/')
    clave = servicios_a_claves[nombre_servicio]
    redis_key_task = 'OML:STATUS_CSV_REPORT:{0}:{1}'.format(clave, task_id)
    return redis_key_task


def es_mensaje_final(message, key_task):
    # TODO: insertar logica mas general para definir si tiene sentido
    # un mensaje final de acuerdo al canal
    if message == b'100':
        return True
    else:
        return False


async def subscribe_to_redis(path):
    conn = await create_connection((REDIS_HOSTNAME, REDIS_PORT))
    key_task = path_to_key(path)

    # Set up a subscribe channel
    channel = Channel(key_task, is_pattern=False)
    await conn.execute_pubsub('subscribe', channel)
    return channel, conn, key_task


async def browser_server(websocket, path):
    channel, conn, key_task = await subscribe_to_redis(path)
    mensaje_final = False
    try:
        while not mensaje_final:
            # Wait until data is published to this channel
            message = await channel.get()
            message_utf8 = message.decode('utf-8')
            # Send unicode decoded data over to the websocket client
            mensaje_final = es_mensaje_final(message, key_task)
            await websocket.send(message_utf8)

    except websockets.exceptions.ConnectionClosed:
        # Free up channel if websocket goes down
        await conn.execute_pubsub('unsubscribe', channel)
        conn.close()
    else:
        conn.close()

if __name__ == '__main__':
    # Runs a server process on 8000.
    loop = asyncio.get_event_loop()
    # loop.set_debug(True)
    ws_server = websockets.serve(browser_server, '0.0.0.0', 8000)
    loop.run_until_complete(ws_server)
    loop.run_forever()
