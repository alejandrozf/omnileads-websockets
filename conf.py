import os

TORNADO_PORT = os.environ.get('TORNADO_PORT') or 8000
REDIS_HOSTNAME = os.environ.get('REDIS_HOSTNAME') or 'redis'
REDIS_PORT = os.environ.get('REDIS_PORT') or 6379
